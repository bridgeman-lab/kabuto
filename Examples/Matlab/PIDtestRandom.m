%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script is a replica of the PIDtest.m script with randomized  %
% velocity targets to validate the effectiveness of the onboard     %
% velocity controller. PIDtest.m is commented to give users a       %
% better understanding of how this code functions.                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear all; clc;
rosshutdown
ipaddress = "http://192.168.2.16:11311";
setenv('ROS_MASTER_URI', ipaddress);
rosinit(ipaddress);
   
robot_0 = Kabuto('robot_0');   

dwell_time = 2; %s
pause(2);

% creating 6 trials of 8 different target velocities
for n = 1:6
    step = 8;
    velocity = zeros(step);
    
    for i = 1:step
        velocity(i) = -600 + rand()*1200;
    end
    
    figure(n); hold on; % Kp = 1, Ki = 0.0003, Kd = 0
    k=1;
    for l = 1:length(velocity)
    int = 20;
    robot_0.right_wheel.writeVel(velocity(l));
    robot_0.left_wheel.writeVel(velocity(l));
    
%     speed_left_hist = zeros(1,int);
%     speed_right_hist = zeros(1,int);
%     vel_target_hist = zeros(1,int);
        
    t = linspace(0,length(velocity)*(int/10),length(velocity)*int);
        while k<(l*length(t)/length(velocity))
            speed_left = robot_0.left_wheel.encoder.readSpeed();
            speed_left_hist(k)=speed_left;
            speed_right = robot_0.right_wheel.encoder.readSpeed();
            speed_right_hist(k)=speed_right;
            vel_target_hist(k)=velocity(l);
            plot(t(1:k), speed_left_hist(1:k), 'r', 'Linewidth', 1); hold on;
            plot(t(1:k), speed_right_hist(1:k), 'b', 'Linewidth', 1); hold on;
            plot(t(1:k), vel_target_hist(1:k), 'k', 'Linewidth', 1.5);
            drawnow
            pause(.1);
            k=k+1;
        end
        
    end
    robot_0.right_wheel.writeVel(0.0);
    robot_0.left_wheel.writeVel(0.0);
    robot_0.right_wheel.writePWM(0);
    robot_0.left_wheel.writePWM(0);
    
    velstr = num2str(n);
    veltitle = strcat('kabuto0_Random_Velocity_Test_', velstr);
    title('Random Velocity Test');
    legend({'Left Wheel', 'Right Wheel'}, 'Location', 'southeast');
    xlabel('Time (s)')
    ylabel('Velocity (mm/s)');
    saveas(gcf, veltitle, 'png');
    pause(5);
end


