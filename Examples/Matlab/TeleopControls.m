classdef TeleopControls < handle
    %TELEOPCONTROLS Summary of this class goes here
    %   Detailed explanation goes here

    properties
        encoder
        motor
    end

    methods
        function obj = TeleopControls(device_name, wheel_pack_name)
            %TELEOPCONTROLS Construct an instance of this class
            %   Detailed explanation goes here

            obj.motor = KabutoPWMOut(device_name, wheel_pack_name);
            obj.encoder = KabutoEncoderInput(device_name, wheel_pack_name);
        end

        function writePWM(obj, val)
            obj.motor.setPWM(val);
        end

        function speed = readSpeed(obj)
            w = obj.encoder.readSpeed();
            speed = w * 1.45;
        end
    end
end




function poses =  TeleopControls(handles)
    keyObj = KeyInput();
    reply = 0;
    
    while reply~='q'
        pwm_right = 0;   % Initialize linear and angular velocities
        pwm_left = 0;      
        reply = getKeystroke(keyObj);
        switch reply
            case 'i'         % i
                pwm_right = 100;
                pwm_left = 100;
            case 'k'     % k
                pwm_right = 100;
                pwm_left = 100;
            case 'j'     % j
                pwm_right = 100;
                pwm_left = 100;
            case 'l'     % l
                pwm_right = 100;
                pwm_left = 100;
        end
        robot_0.right_wheel.writePWM(pwm_right);
        robot_0.left_wheel.writePWM(pwm_left);
    end

    poses = handles.poses;
    closeFigure(keyObj);
end