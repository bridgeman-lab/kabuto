%% Initialize Matlab as a ROS node
rosshutdown
ipaddress = "http://192.168.2.16:11311";
setenv('ROS_MASTER_URI', ipaddress);
rosinit(ipaddress);

%% Initialize robots
%robot_0 = Kabuto('robot_0');   
 robot_1 = Kabuto('robot_1'); %<-- uncomment to run both robots

dwell_time = 2; %s
pause(2);


%% Main script here
% robot_0.right_wheel.writeVel(200.0);
% robot_0.left_wheel.writeVel(200.0);
% pause(10);
% robot_0.right_wheel.writeVel(500.0);
% robot_0.left_wheel.writeVel(500.0);
% pause(10);
% robot_0.right_wheel.writeVel(200.0);
% robot_0.left_wheel.writeVel(200.0);
% pause(10);
% robot_0.right_wheel.writeVel(-300.0);
% robot_0.left_wheel.writeVel(-300.0);
% pause(10);
% robot_0.right_wheel.writeVel(0.0);
% robot_0.left_wheel.writeVel(0.0);
% 
% robot_0.right_wheel.writePWM(0); % <-- controller may cause vel of 0.0 to oscillate, send pwm = 0 as well to turn robot off.
% robot_0.left_wheel.writePWM(0);

% val=robot_1.IMU.readIMU();

robot_1.right_wheel.writeVel(0.0);
robot_1.left_wheel.writeVel(0.0);
