%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script is an adaptation of the exampleHelperTurtleBotKeyboardControl
% function found in Matlab's codebase. See also: ExampleHelperTurtleBotKeyInput.m
% More info can be found here:
% https://www.mathworks.com/help/ros/ug/control-the-turtlebot-with-teleoperation.html
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rosshutdown
ipaddress = "http://192.168.2.16:11311";
setenv('ROS_MASTER_URI', ipaddress);
rosinit(ipaddress);

robot_0 = Kabuto('robot_0');
% robot_1 = Kabuto('robot_1');


k=0;

keyObj = KeyInput();
reply = 0;
    
    while reply~='q'
        pwm_right = 0;   % Initialize linear and angular velocities
        pwm_left = 0;      
        reply = getKeystroke(keyObj);
        switch reply
            case 'i'         % i
                pwm_right = 100;
                pwm_left = 100;
            case 'k'     % k
                pwm_right = -100;
                pwm_left = -100;
            case 'j'     % j
                pwm_right = 100;
                pwm_left = -100;
            case 'l'     % l
                pwm_right = -100;
                pwm_left = 100;
        end
        robot_0.right_wheel.writePWM(pwm_right);
        robot_0.left_wheel.writePWM(pwm_left);
    end

poses = handles.poses;
closeFigure(keyObj);
