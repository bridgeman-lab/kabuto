close all; clear all; clc;
rosshutdown % make sure Matlab ROS nodes are closed
ipaddress = "http://192.168.2.16:11311";
setenv('ROS_MASTER_URI', ipaddress);
rosinit(ipaddress); % initialize ROS node

% Name robot and classify as Kabuto
robot_0 = Kabuto('robot_0');   
pause(2); % wait to make sure robot is set up

robot_0.right_wheel.writeVel(300.0);
robot_0.left_wheel.writeVel(300.0);

robot_0.right_wheel.encoder.readSpeed();
robot_0.left_wheel.encoder.readSpeed();