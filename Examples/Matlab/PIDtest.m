%% Initialize Matlab as a ROS node
close all; clear all; clc;
rosshutdown
ipaddress = "http://192.168.2.16:11311";
setenv('ROS_MASTER_URI', ipaddress);
rosinit(ipaddress);

%% Initialize robots
robot_0 = Kabuto('robot_0');   

dwell_time = 2; %s
pause(2);

%% Set target velocity and plot to see performance of PID controller (onboard)
velocity = [400 -400];
for l = 1:length(velocity)
    robot_0.right_wheel.writeVel(velocity(l));
    robot_0.left_wheel.writeVel(velocity(l));
    int = 40;
    
    speed_left_hist = zeros(1,int);
    speed_right_hist = zeros(1,int); 
    
    % Create figure to graph speed
    figure(l); hold on; % Kp = 1, Ki = 0.0003, Kd = 0
    t = linspace(0,int/10,int);
    k=1;
    while k<(int+1)
        tic
        speed_left = robot_0.left_wheel.encoder.readSpeed(); % reading speed of left wheel (mm/s)
        speed_left_hist(k)=speed_left;
        speed_right = robot_0.right_wheel.encoder.readSpeed(); % reading speed of right wheel (mm/s)
        speed_right_hist(k)=speed_right;
        plot(t(1:k), speed_left_hist(1:k), 'r', 'Linewidth', 1); hold on;
        plot(t(1:k), speed_right_hist(1:k), 'b', 'Linewidth', 1);
        while toc<0.1 % wait until 0.1 second has elapsed
        end
dwell_time = 2; %s
        k=k+1;
    end
    velstr = num2str(velocity(l));
    veltitle = strcat('kabuto0_velocity_FF_',velstr);
    title(['Velocity Desired = ' velstr]);
    legend({'Left Wheel', 'Right Wheel'}, 'Location', 'southeast');
    xlim([0 4]);
    xlabel('Time (s)')
    ylabel('Velocity (mm/s)');
    saveas(gcf, veltitle, 'png');
    
    % Send velocity and pwm to zero to make sure robot stops moving
    robot_0.right_wheel.writeVel(0.0);
    robot_0.left_wheel.writeVel(0.0);
    robot_0.right_wheel.writePWM(0);
    robot_0.left_wheel.writePWM(0);
    pause(2);
end
