%% replicating Lavaei et al. 2021 numerical example with Kabuto robots
close all; clearvars; clc;

%% initialization of controller
% loading pre-computed controllers from swModeControllers.m
addpath '/home/bridgemanlab/qbot-project/Lavaei_etal_2021_code'
load('swControllersBuilt.mat', 'controllers', 'K'); 

a = 0.15; % setting angular scaling so that robots don't run out of space

% hardcoded to match numerical example in paper
mode_signal = [ones(1,21), 2*ones(1,43), ones(1,10), 2*ones(1,20), ones(1,55)];

% tracking timesteps
timestep=0.1; %seconds -- previously defined in controller
num_timesteps = length(mode_signal);

% keeping track of dwell times
mdt = [20, 10];
dwell_time = ones(1,num_timesteps);
for i=1:num_timesteps-1
    if mode_signal(i+1) ~= mode_signal(i)
        dwell_time(i+1) = 1;
    else
        dwell_time(i+1) = min(mdt(mode_signal(i+1)), dwell_time(i)+1);
    end
end

% create arrays to keep track of states & inputs for plotting
x_plot = zeros(num_timesteps,3);
u_plot = zeros(num_timesteps,2);

%% initialize ROS and robots
rosshutdown
ipaddress = "http://192.168.2.16:11311";
setenv('ROS_MASTER_URI', ipaddress);
rosinit(ipaddress);

robot_0 = Kabuto('robot_0');
robot_1 = Kabuto('robot_1');

pause(2);  % wait for ROS to instantiate and start collecting data
%% applying controller policy to robots
% set robots at initial velocities
v0 = 300;
v0_r = (1+a)*v0;
v0_l = (1-a)*v0;

robot_0.right_wheel.writeVel(v0_r);
robot_0.left_wheel.writeVel(v0_l);
robot_1.right_wheel.writeVel(v0_r);
robot_1.left_wheel.writeVel(v0_l);
pause(5);

d0=2000; % set initial following distance (mm)

% get initial encoder reading to get relative position
rob0_pos0_r = robot_0.right_wheel.encoder.readPos();
rob0_pos0_l = robot_0.left_wheel.encoder.readPos();
rob1_pos0_r = robot_1.right_wheel.encoder.readPos();
rob1_pos0_l = robot_1.left_wheel.encoder.readPos();

for i=1:num_timesteps-1
    tic;
    % collecting and calculating current state values -- x = [d, v_rob0, v_rob1]
    rob0_pos_r = robot_0.right_wheel.encoder.readPos() - rob0_pos0_r;
    rob0_pos_l = robot_0.left_wheel.encoder.readPos() - rob0_pos0_l;
    rob0_speed_r = robot_0.right_wheel.encoder.readSpeed();
    rob0_speed_l = robot_0.left_wheel.encoder.readSpeed();

    rob1_pos_r = robot_1.right_wheel.encoder.readPos() - rob1_pos0_r;
    rob1_pos_l = robot_1.left_wheel.encoder.readPos() - rob1_pos0_l;
    rob1_speed_r = robot_1.right_wheel.encoder.readSpeed();
    rob1_speed_l = robot_1.left_wheel.encoder.readSpeed();
    
    % calculating state to feed to controller
    d = d0+((rob0_pos_l + rob0_pos_r)/2 - (rob1_pos_l + rob1_pos_r)/2);
    v_rob0 = ((rob0_speed_r + rob0_speed_l)/2); 
    v_rob1 = ((rob1_speed_r + rob1_speed_l)/2);

    % rescaling and aligning state for controller
    x = [double(d); v_rob0; v_rob1];
    x_re = 0.001*x;

    % applying controller policy for timestep
    ctrl_result = controllers{1,mode_signal(i)}{1,dwell_time(i)}(x_re);

    % augmenting new controller-calculated velocities to send to robots
    u_nom = ctrl_result(1:2);
    x_nom = ctrl_result(3:5);

    u = (u_nom + K*(x - x_nom))*1000; % augmenting and rescaling for robot

    % change accelerations to velocities
    v_new = [v_rob0; v_rob1] + u*timestep;

    % generalized velocity for left and right wheel (drive in circle so
    % we dont run out of space)
    u_to_rob0_r = (1+a)*v_new(1);
    u_to_rob0_l = (1-a)*v_new(1);
    u_to_rob1_r = (1+a)*v_new(2);
    u_to_rob1_l = (1-a)*v_new(2);

    % sending new velocities to robots[
    robot_0.right_wheel.writeVel(u_to_rob0_r);
    robot_0.left_wheel.writeVel(u_to_rob0_l);
    robot_1.right_wheel.writeVel(u_to_rob1_r);
    robot_1.left_wheel.writeVel(u_to_rob1_l);

    % replace d with d0
    d0 = d;
    
    % TODO: Add logging to track states and inputs for plotting (run, then
    % plot to save time during control loop)
    x_plot(i,:) = x;
    u_plot(i,:) = u;

    while toc < 0.1
        % Spin until 0.1 seconds are spent
    end
    disp(toc);
end

% Stop robots and end script
robot_0.right_wheel.writeVel(0.0);
robot_0.left_wheel.writeVel(0.0);
robot_0.right_wheel.writePWM(0);
robot_0.left_wheel.writePWM(0);
robot_1.right_wheel.writeVel(0.0);
robot_1.left_wheel.writeVel(0.0);
robot_1.right_wheel.writePWM(0);
robot_1.left_wheel.writePWM(0);