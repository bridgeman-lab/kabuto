function pre_set = getPreSetU(A, B, X, U)
pre_set = (X + (-B*U))*A;
pre_set.minVRep;
pre_set.minHRep;
pre_set.normalize;
assert(pre_set.isFullDim)
end