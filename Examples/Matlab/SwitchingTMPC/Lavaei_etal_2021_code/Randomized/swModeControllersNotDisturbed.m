clc
clear
close all

load setOps.mat

% %Cost funtion weights
% q1 = 0.1; %Cost of distance error
% q2 = 1;  %Cost of velocity error
% R = 0.1*[1 0; 0 1]; %Cost of accelerating

nx = size(A,1); % Number of states
nu = size(B,2); % Number of inputs

%% Building controllers of each mode in x
    controllers = cell(1,2); % one controller for each mode
    controllers{1} = cell(1,d(1));
    controllers{2} = cell(1,d(2));
    
for i = 1:size(CIsets, 1)
    yalmip('clear')
    
    u = sdpvar(nu*ones(1,N(i)),ones(1,N(i))); % nominal control vector
    x = sdpvar(nx*ones(1,N(i)+1),ones(1,N(i)+1)); % nominal state vector
    q = sdpvar(3,1);
    
    ops = sdpsettings('verbose', 0, 'CACHESOLVERS', 1);
    
    % i = 1 => High speed
    % i = 2 => Low Speed
    
    zr = zRef(i);
    vr = vRef(i);
    
    for ts = 0:d(i)
        
        % at k = 1
        constraints = [
            (sets{2,i}.A)*u{1}<=sets{2,i}.b, x{2} == A*x{1}+B*u{1}];
        objective = q(1)*(x{1}(1)-zr)^2/(x{1}(1)-vr)...
            +q(2)*((x{1}(2)-vr)^2+(x{1}(3)-vr)^2)...
            +u{1}'*q(3)*u{1};
        if ts == d(i) % this handles when w = 0 is used
            constraints = [constraints; CSIinf(i,1).A*x{1}<=CSIinf(i,1).b];
        end
        
        % adding running cost and constraints
        for k=2:N(i)
            objective = objective+q(1)*(x{k}(1)-zr)^2/(x{k}(1)-vr)...
                +q(2)*((x{k}(2)-vr)^2+(x{k}(3)-vr)^2)...
                +u{k}'*q(3)*u{k};
            constraints = [constraints; x{k+1}==A*x{k}+B*u{k},... %Dynamic Constraint
                sets{2,i}.A*u{k}<=sets{2,i}.b];  %Input Constraint
            if k < d(i)-ts+1
                constraints = [constraints; sets{1,i}.A*x{k}<=sets{1,i}.b]; %State Constraint
            else % k >= d(i)-ts+1
                constraints = [constraints; CSIinf(i,1).A*x{k}<=CSIinf(i,1).b]; %State Constraint
            end
        end
        
        % adding terminal objective
        objective = objective + q(1)*(x{N(i)+1}(1)-zr)^2/(x{N(i)+1}(1)-vr)...
            +q(2)*((x{N(i)+1}(2)-vr)^2+(x{N(i)+1}(3)-vr)^2);
        
        
        % adding terminal constraints
        if d(i)-ts-N(i) > 0
            temp = d(i)-ts-N(i);
            setTemp = CSIinf(i,1);
            for j = 1:temp
                setTemp = getPreSetU(A, B, setTemp, sets{2, i}) & sets{1, i};
                setTemp = makeProper(setTemp);
            end
            constraints = [constraints;setTemp.A*x{N(i)+1} <= setTemp.b];
        else
            constraints = [constraints; CSIinf(i,1).A*x{N(i)+1} <= CSIinf(i,1).b];
        end
        
        controllers{i}{ts+1} = optimizer(constraints,objective,ops,[x{1};q],u{1});
        clear constraints
    end
end

save swControllersBuiltDeterministic.mat
