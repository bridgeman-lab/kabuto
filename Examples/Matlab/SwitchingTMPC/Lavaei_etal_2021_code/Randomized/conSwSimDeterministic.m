clc
clear
close all

load swControllersBuiltDeterministic.mat

%% Simulation

% load the sequence which was infeasible for naive design
load('mySwitchingSequence.mat','mode','xInit');
[~,ind] = min(mode);
xInit = [1 0.2 0.5]';
% q = [0.1 0.05 1]';
q = [0.2 0.1 1]';
% mode(ind:end) = mode(ind-1);

T = 150; % number of simulation steps

% feasible initial conditions of mode 2
F2 = dStepPreSetsCSI{mode(1),1};
assert(min(F2.A*xInit <= F2.b)>=1)

tNum = 100;
XX = {};
WW = {};

for r = 1:tNum
    fprintf('r is: %d\n',r)
    Xs = zeros(nx,T); % Initializing the vector of all states
    Xs(:,1) = xInit;
    Ux = zeros(nu,T-1); % Initializing the vector of all control inputs
    ww = zeros(3,T);
    I = 0; % infeasiblity flag
    contNum = zeros(1,T); % which controller to use?
    ts = 0; % time since last switch
    
    for i = 1:T
        
        if  i == ind - 1
            mode(i) = -mode(i-1)+3;
            ts = 0;
            disp("sw")
        end
        
        if i > ind - 1 && i < 65
            mode(i) = mode(i-1);
        end
        
        if i == 65
            mode(i) = -mode(i-1)+3;
            ts = 0;
            disp("sw")
        end
        
        if i > 65 && i < 75
            mode(i) = mode(i-1);
        end
        
        if i == 75
            mode(i) = -mode(i-1)+3;
            ts = 0;
            disp("sw")
        end
        
        if i > 75 && i < 95
            mode(i) = mode(i-1);
        end
        
        if i == 95
            mode(i) = -mode(i-1)+3;
            ts = 0;
            disp("sw")
        end
        
        if i > 95 && i <= T
            mode(i) = mode(i-1);
        end
        
        contNum(i) = min(ts+1,d(mode(i))+1);
        
        Ux(:,i) = controllers{1,mode(i)}{1,contNum(i)}([Xs(:,i);q]);
        
        if isnan(Ux(1,i))
            I = i;
            fprintf('infeasible at i = %d\n',I);
            XX = [XX; {Xs, I}];
            WW = [WW; {ww, I}];
            break;
        end
        
        ww(:,i) = 0.01*rand(3,1) -0.005*ones(3,1);
        %     ww(:,i) = - 0.005*ones(3,1);
        Xs(:,i+1) = A*Xs(:,i) + B*Ux(:,i) + ww(:,i);
        ts = ts + 1;
    end
end

% if I ~= 0
%     T = I;
% end
%
%
% figure
% stairs(1:T,Xs(1,1:T))
% % title('unconstrained switches')
% xlabel('t')
% ylabel('s (m)')
% grid on
% hold on
% for j = 1:T
%     if mode(j) == 2
%         plot(j,Xs(1,j),'g.')
%     else
%         plot(j,Xs(1,j),'r.')
%     end
% end

% save conResults2019Mod.mat


%% Check the feasibility
setTemp = CSIinf(2,1);
% for j = 1:2
%     setTemp = getPreSetU(A, B, setTemp, sets{2, 2}) & sets{1, 2};
%     setTemp = makeProper(setTemp);
% end
min(setTemp.A*Xs(:,I) <= setTemp.b)

% figure
% plot(CSIinf(2,1), 'alpha', 1 , 'color', 'b');
% hold on
% plot(setTemp, 'alpha', 0.5 , 'color', 'c');
% xlabel('z')
% ylabel('v_1')
% zlabel('v_2')