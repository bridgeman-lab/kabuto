clc
clear
close all

% size of image (pixels)
width = 600;
height = 350;

% Prepare
figure('Position', [100 100 width height])
hold on

load conResults.mat T zRef

swInds = [0 22-1 65-1 75-1 95-1];

h = [];

%% safe zones

% disturbed
m1_lb = 0.53;
m1_ub = 10;
m2_lb = 0.4;
m2_ub = 5;

% nominal
m1_lb_n = 0.59;
m1_ub_n = 9.94;
m2_lb_n = 0.46;
m2_ub_n = 4.94;

for i = 1:length(swInds)
    if mod(i,2) == 1
        curMode = 2;
        lb = m2_lb;
        ub = m2_ub;
        lbn = m2_lb_n;
        ubn = m2_ub_n;
    elseif mod(i,2) == 0
        curMode = 1;
        lb = m1_lb;
        ub = m1_ub;
        lbn = m1_lb_n;
        ubn = m1_ub_n;
    end

    if i == length(swInds)
        tTemp = swInds(i):T;
    else
        tTemp = swInds(i):swInds(i+1);
    end
    
    % disturbed safe zone
    t2 = [tTemp, fliplr(tTemp)];
    inBetween1 = [lb*ones(1,length(tTemp)), fliplr(ub*ones(1,length(tTemp)))];
    a1 = fill(t2, inBetween1,'g','LineStyle','none');
    alpha(0.15)
    h = [h a1];
    
    % nominal safe zone
    inBetween2 = [lbn*ones(1,length(tTemp)), fliplr(ubn*ones(1,length(tTemp)))];
    a2 = fill(t2, inBetween2,'g','LineStyle','none');
    alpha(0.15)
    h = [h a2];
    
    % zRef
    tq = [tTemp(1), tTemp(end)];
    a3 = plot(tq,[zRef(curMode),zRef(curMode)],'color',[0.4660 0.6740 0.1880],'LineWidth',1.2); % green
    h = [h a3];
    
    % swithing times
    a4 = plot([swInds(i) swInds(i)],[0.2 2],'k-.','LineWidth',0.5);
    h = [h a4];
end

% h has 20 elements here

%% Plot trajectories
% Plot constrained X
clearvars -except h swInds
load conResults.mat
a = plot(0:T-1,Xs(1,1:T),'b','LineWidth',1);
h = [h a];

% Plot constrained nominal X
a = plot(0:T-1,xStar0(1,1:T),'b--','LineWidth',1);
h = [h a];
clearvars -except h swInds


% plot naive X
load mySwitchingSequence.mat
a = plot(0:I-1,Xs(1,1:I),'r','LineWidth',1);
h = [h a];

% plot naive nominal X
a = plot(0:I-1,xStar0(1,1:I),'r--','LineWidth',1);
h = [h a];

% h has 24 elements

%% Legends

leg = legend([h(23),h(24),h(21),h(22),h(4),h(3)],{'$s,\textrm{ naive design}$','$\bar{s},\textrm{ naive design}$','$s,\textrm{ Theorem 2 design}$','$\bar{s},\textrm{ Theorem 2 design}$','$\textrm{Switching times}$','$\textrm{Desired value}, s_r$'});
% leg = legend([h(23),h(24),h(21),h(22),h(3)],{'$s,\textrm{ naive design}$','$\bar{s},\textrm{ naive design}$','$s,\textrm{ Theorem 2 design}$','$\bar{s},\textrm{ Theorem 2 design}$','$\textrm{Desired value}, s_r$'});
set(leg,'Interpreter','latex');
set(leg,'FontSize',10);
set(leg,'Location','southeast')

xlabel('$t \textrm{ (steps)}$','Interpreter','latex')
xticks([swInds 149])
ylabel('\textrm{Distance between vehicles (m)}','Interpreter','latex')
xlim([-3,149])
ylim([0.37,1])
% grid on

% %% to tikz
% cleanfigure
% matlab2tikz('myfile.tex')

%% Print
% print('compareImplement', '-dpng', '-r300');