clc
clear
close all

load swControllersBuilt.mat

%% Simulation

% load the sequence which was infeasible for naive design
load('mySwitchingSequence.mat');
[~,ind] = min(mode);
% mode(ind:end) = mode(ind-1);

T = 150; % number of simulation steps

ww = zeros(3,T);
% ww = w;

% feasible initial conditions of mode 2
F2 = dStepPreSetsCSI{mode(1),1} + S;
assert(min(F2.A*xInit <= F2.b)>=1) 

Xs = zeros(nx,T); % Initializing the vector of all states
Xs(:,1) = xInit;

Ux = zeros(nu,T-1); % Initializing the vector of all control inputs
UxStar = zeros(nu,T-1); % Initializing the vector of all control inputs for the nominal system

xStar0 = zeros(nx,T); % Initializing the vector of all nominal states

I = 0; % infeasiblity flag

contNum = zeros(1,T); % which controller to use?
ts = 0; % time since last switch

tic
for i = 1:T
    
    if  i == ind - 1 
        mode(i) = -mode(i-1)+3;
        ts = 0;
        disp("sw")
    end
    
    if i > ind - 1 && i < 65
        mode(i) = mode(i-1);
    end
    
    if i == 65
        mode(i) = -mode(i-1)+3;
        ts = 0;
        disp("sw")
    end
    
    if i > 65 && i < 75
        mode(i) = mode(i-1);
    end
    
    if i == 75
        mode(i) = -mode(i-1)+3;
        ts = 0;
        disp("sw")
    end
    
    if i > 75 && i < 95
        mode(i) = mode(i-1);
    end
    
    if i == 95
        mode(i) = -mode(i-1)+3;
        ts = 0;
        disp("sw")
    end
    
    if i > 95 && i <= T
        mode(i) = mode(i-1);
    end
    
%     if i > ind - 1 && i < 200
%         if ts >= d(2) && mode(i-1) == 2 && Xs(1,i) > Xs(1,i-1)
%             disp("sw")
%             mode(i) = 1;
%             ts = 0;
%         elseif ts >= d(1) && mode(i-1) == 1 && Xs(1,i) < Xs(1,i-1)
%             disp("sw")
%             mode(i) = 2;
%             ts = 0;
%         elseif i > 1
%             mode(i) = mode(i-1);
%         end
%     end
    

    
    contNum(i) = min(ts+1,d(mode(i)));
    
    answer = controllers{1,mode(i)}{1,contNum(i)}(Xs(:,i));

    UxStar(:,i) = answer(1:nu);
    xStar0(:,i) = answer(nu+1:end);
    
    if isnan(UxStar(1,i))
        I = i;
        fprintf('infeasible at i = %d\n',i);
        iEnd = i - 1;
        Xinfeas = Xs(:,1:iEnd);
        break;
    end
    
    Ux(:,i) = UxStar(:,i) + K*(Xs(:,i)-xStar0(:,i));
    
    ww(:,i) = 0.01*rand(3,1) -0.005*ones(3,1);
    Xs(:,i+1) = A*Xs(:,i) + B*Ux(:,i) + ww(:,i);
    
    ts = ts + 1;
    
end
toc

if I ~= 0
    T = I;
end


figure
stairs(1:T,Xs(1,1:T))
% title('unconstrained switches')
xlabel('t')
ylabel('s (m)')
grid on
hold on
for j = 1:T
    if mode(j) == 2
        plot(j,Xs(1,j),'g.')
    else
        plot(j,Xs(1,j),'r.')
    end
end

stairs(1:T,xStar0(1,1:T),'k')

% save conResults.mat
