function y = alphaS(A,fMat,gVec,I,s)

As = A^s;

ytemp = hW(As'*fMat(1,:)',fMat,gVec)/gVec(1);

if I == 1
    y = ytemp;
else
    for i = 2:I
        ytemp = max(ytemp,hW(As'*fMat(i,:)',fMat,gVec)/gVec(i));
    end
    y = ytemp;
end