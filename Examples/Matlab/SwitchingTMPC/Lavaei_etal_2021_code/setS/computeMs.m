function y = computeMs(A,s,fMat,gVec)

n = size(fMat,2);
ytemp = zeros(n,1);

for j = 1:n
    e = zeros(n,1);
    e(j) = 1;
    term1 = 0;
    term2 = 0;
    for i = 0:s-1
        term1 = term1 + hW((A^i)'*e,fMat,gVec);
        term2 = term2 + hW(-(A^i)'*e,fMat,gVec);
    end
    ytemp(1) = max(term1,term2);
end
    
y = max(ytemp);
