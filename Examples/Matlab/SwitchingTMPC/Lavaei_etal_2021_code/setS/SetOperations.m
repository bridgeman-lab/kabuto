clc
clear
close all


%% Model dynamics

% Sampling time
Ts = 0.1;

A = [1 Ts -Ts; 0 1 0; 0 0 1];
B = [Ts^2/2 -Ts^2/2; Ts 0; 0 Ts];


%% Findind AK
QQ = 1*eye(3);  % 1
RR = 0.01*eye(2); % 0.01
[P,~,K] = dare(A,B,QQ,RR);
K = -K; % K is same for the two modes because of same cost weights
AK = A + B*K;

%% Constraint on W (always at the origin of the each coordinate system?)
% I can make it a rectangular!
wBound = [0.01 0.01 0.01];
W = Polyhedron([eye(3); -eye(3)], [wBound wBound]');
W = makeProper(W);

%% Making S (The same for two modes because w is same)
fMat = W.A;
gVec = W.b;
I = length(gVec);

epsilon = 0.001 ; % 0.1

s = 0;
alpha = 1;
Ms = epsilon; % only for initializing the loop

while alpha > epsilon/(epsilon + Ms)
    s = s + 1;
    alpha = alphaS(AK,fMat,gVec,I,s);
    Ms = computeMs(AK,s,fMat,gVec);
    fprintf('*** %d ****\n',s);
end



Fs1 = W;
ALast = eye(size(AK,1));
for i = 1:floor((s-1)/3)
    ANew = ALast*AK;
    Fs1 = Fs1 + ANew*W;
    ALast = ANew;
    if mod(i,5)==0
     Fs1 = makeProper(Fs1);
    end
    i
end



for i = floor((s-1)/3)+1:floor(2*(s-1)/3)
    ANew = ALast*AK;
    if i == floor((s-1)/3)+1
        Fs2 = ANew*W;
    else
        Fs2 = Fs2 + ANew*W;
    end
    ALast = ANew;
    if mod(i,5)==0
     Fs2 = makeProper(Fs2);
    end
    i
end


for i = floor(2*(s-1)/3)+1:s-1
    ANew = ALast*AK;
    if i == floor(2*(s-1)/3)+1
        Fs3 = ANew*W;
    else
        Fs3 = Fs3 + ANew*W;
    end
    ALast = ANew;
    if mod(i,5)==0
     Fs3 = makeProper(Fs3);
    end
    i
end

disp('yes!')
% save FsParts.mat

Fs12 = Fs1 + Fs2;
Fs12 = makeProper(Fs12);
disp('Fs2 done!')
% save Fs12.mat
Fs3 = makeProper(Fs3);
disp('properedFs3!')
Fs123 = Fs12 + Fs3;
% Fs = Fs123;
Fs = makeProper(Fs123);
disp('Fs done!')


S = 1/(1 - alpha)*Fs;
assert(~S.isEmptySet())
assert(S.isFullDim())

save computedS.mat 


plot(S)
figure
plot(K*S,'color','red')

% load computedS.mat

% %% High and low velocity reference point
% %  high velocity mode: mode 1
% %  low velocity mode: mode 2
% vRef = [0.4; 0.3];
% zRef = 2.25*vRef;
% xs1 = [zRef(1) vRef(1) vRef(1)]';
% xs2 = [zRef(2) vRef(2) vRef(2)]';
% xs = [xs1 xs2];
% 
% %% Constraints on X , U and W in the two modes
% X1 = Polyhedron([eye(3); -eye(3)], [10; 1; 1; -4/3*vRef(1); -.2; -.2]);
% X2 = Polyhedron([eye(3); -eye(3)], [5; 0.8; 0.8; -4/3*vRef(2); 0.2; 0.2]);
% % U1 = Polyhedron([eye(2); -eye(2)], [0.1 0.1 0.3 0.3]');
% % U2 = Polyhedron([eye(2); -eye(2)], [0.25 0.25 0.3 0.3]');
% U1 = Polyhedron([eye(2); -eye(2)], [0.5 0.5 0.6 0.6]');
% U2 = Polyhedron([eye(2); -eye(2)], [0.4 0.4 0.6 0.6]');
% 
% X1 = makeProper(X1);
% X2 = makeProper(X2);
% U1 = makeProper(U1);
% U2 = makeProper(U2);
% 
% figure
% plot(U1-K*S,'color','green')
% % hold on
% % plot(K*S,'color','red')
% 
% figure
% plot(U2-K*S,'color','green')
% % hold on
% % plot(K*S,'color','red')
% 
% figure
% plot(X1-S)
% 
% figure
% plot(X2-S)