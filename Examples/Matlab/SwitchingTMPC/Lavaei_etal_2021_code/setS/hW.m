function y = hW(a,fMat,gVec)


w = sdpvar(length(a),1);

constraint = fMat*w <= gVec;
objective = a'*w; % Minus because we need supremum
ops = sdpsettings('verbose',0);
optimize(constraint, -objective,ops);

y = value(objective);