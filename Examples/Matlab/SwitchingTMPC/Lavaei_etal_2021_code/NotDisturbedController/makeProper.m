function polOut = makeProper(polIn)

polIn.minHRep;
polIn.minVRep;
polIn.normalize;

polOut = polIn;
end