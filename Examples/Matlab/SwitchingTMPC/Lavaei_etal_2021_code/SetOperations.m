clc
clear
close all

load computedS.mat

%% High and low velocity reference point
%  high velocity mode: mode 1
%  low velocity mode: mode 2
vRef = [0.4; 0.3]; 
zRef = 2.25*vRef;
xs1 = [zRef(1) vRef(1) vRef(1)]';
xs2 = [zRef(2) vRef(2) vRef(2)]';
xs = [xs1 xs2]; 

%% Constraints on X , U and W in the two modes
X1 = Polyhedron([eye(3); -eye(3)], [10; 0.6; 0.6; -4/3*vRef(1); -0.2; -0.2]);
X2 = Polyhedron([eye(3); -eye(3)], [5; 0.5; 0.5; -4/3*vRef(2); 0.2; 0.2]);
% U1 = Polyhedron([eye(2); -eye(2)], [0.1 0.1 0.3 0.3]');
% U2 = Polyhedron([eye(2); -eye(2)], [0.25 0.25 0.3 0.3]');
U1 = Polyhedron([eye(2); -eye(2)], [0.1 + 0.1613 0.1 + 0.1613 0.3 + 0.1613 0.3 + 0.1720]');
U2 = Polyhedron([eye(2); -eye(2)], [0.25 + 0.1613 0.25 + 0.1613 0.3 + 0.1613 0.3 + 0.1720]');

X1 = makeProper(X1);
X2 = makeProper(X2);
U1 = makeProper(U1);
U2 = makeProper(U2);

%% Cost funtion weights
% These will change later in the controller!
q1 = 0.25; % Cost of distance error
q2 = 0.25;  % Cost of velocity error
R = 1.1*[1 0; 0 1]; % Cost of acceleration
Q = [q1 0 0;0 q2 0;0 0 q2];


% plot(S)
% xlabel('x1')
% ylabel('x2')
% zlabel('x3')
% figure
% plot(U1,'color','green')
% hold on
% plot(K*S,'color','red')
% title('U1 and K*Z')
% figure
% plot(U1-K*S)
% title('U1 \ominus K*Z')
% 
% figure
% plot(U2,'color','green')
% hold on
% plot(K*S,'color','red')
% title('U2 and K*Z')
% figure
% plot(U2-K*S)
% title('U2 \ominus K*Z')



%% Tightened constraints
X1bar = makeProper(X1 - S);
X2bar = makeProper(X2 - S);
U1bar = makeProper(U1 - K*S);
U2bar = makeProper(U2 - K*S);

assert(~X1bar.isEmptySet())
assert(~U1bar.isEmptySet())
assert(~X2bar.isEmptySet())
assert(~U2bar.isEmptySet())

sets = {X1bar, X2bar; U1bar, U2bar};

%% Plot S1, S2 and Constraints in the corresponding coordinates
% S1 and S2 is centered on the origin (the same in all coordiantes)
% figure
% plot(S)
% xlabel('z');
% ylabel('v_1');
% zlabel('v_2');
% title('S1')

% plot X1 and X1bar
figure
plot(X1,'alpha',0.4,'color','y')
hold on
plot(X1bar,'alpha',0.4,'color','r')
plot(S,'alpha',0.8,'color','r')
xlabel('z')
ylabel('v_1')
zlabel('v_2')
legend('$X_1$','$\bar{X}_1$','$S$','interpreter','latex')

% This is plotted in the e2 coordinates
figure
plot(X2,'alpha',0.4,'color','c')
hold on
plot(X2bar,'alpha',0.4,'color','b')
plot(S,'alpha',0.8,'color','r')
xlabel('z')
ylabel('v_1')
zlabel('v_2')
legend('$X_2$','$\bar{X}_2$','$S$','interpreter','latex')

%% Computing maxCSIsets

% load CSIsetsFound.mat CSIinf1 CSIinf2 CSIinf

%MDT
d1 = 20;
d2 = 10;

cur_set1 = X1bar;
cur_set2 = X2bar;
fprintf("Finding Control Switch Invariant Sets \n");

i = 1;

while 1

    next_set1 = (getPreSetU(A, B, cur_set1, U1bar) & X1bar) & cur_set1; % in x
    next_set1 = makeProper(next_set1); % in x
    
    next_set2 = (getPreSetU(A, B, cur_set2, U2bar) & X2bar) & cur_set2; % in x
    next_set2 = makeProper(next_set2); % in x

    pre1 = cur_set1; % in x
    pre2 = cur_set2; % in x

    for k=1:d1
        pre1 = getPreSetU(A, B, pre1, U1bar) & X1bar;
        pre1 = makeProper(pre1);
        assert(pre1.isFullDim)
    end
    
    for k=1:d2
        pre2 = getPreSetU(A, B, pre2, U2bar) & X2bar;
        pre2 = makeProper(pre2);
        assert(pre2.isFullDim)
    end
    
    % Now find the intersections in x
    next_set1 = next_set1 & pre2;
    next_set1 = makeProper(next_set1);
    assert(next_set1.isFullDim)

    next_set2 = next_set2 & pre1;
    next_set2 = makeProper(next_set2);
    assert(next_set2.isFullDim)
    
    % Check if we have convered
    if next_set1 == cur_set1 && next_set2 == cur_set2
        fprintf("| \nConverged in %d iterations\n", i);
        CSIinf1 = cur_set1;
        CSIinf2 = cur_set2;
        CSIinf = [CSIinf1;CSIinf2];
        break;
    end

    cur_set1 = next_set1;
    cur_set2 = next_set2;

    i
    i = i + 1;
end

save CSIsetsFound.mat CSIinf1 CSIinf2 CSIinf
d = [d1 d2];

%% plot CSIsets
figure
plot(X1bar, 'alpha', 1 , 'color', 'y');
hold on
plot(CSIinf(1,1), 'alpha', 0.5 , 'color', 'r');
xlabel('z')
ylabel('v_1')
zlabel('v_2')
legend('$\bar{X}_1$','$CSI^\infty_1$','Interpreter','latex')
% print('', '-dpng', '-r400')

figure
plot(X2bar, 'alpha', 1 , 'color', 'c');
hold on
plot(CSIinf(2,1), 'alpha', 0.5 , 'color', 'b');

xlabel('z')
ylabel('v_1')
zlabel('v_2')
legend('$\bar{X}_2$','$CSI^\infty_2$','Interpreter','latex')
% print('', '-dpng', '-r400')


%% Find the MaxCIsets of each mode in x
CIsets = cell(2,1);

% chop off for mode 1 in coordinate e1
cur_set = X1bar;
i = 1;

while 1
    next_set = (getPreSetU(A, B, cur_set, U1bar) & X1bar) & cur_set;
    next_set = makeProper(next_set);

    if next_set == cur_set
        fprintf(" \nConverged in %d iterations\n", i);
        CIsets{1,1} = cur_set;
        break;
    end

    cur_set = next_set;
    i
    i = i + 1;
end

assert(~CIsets{1,1}.isEmptySet())


% chop off for mode 2 in coordinate e2
cur_set = X2bar;
i = 1;

while 1
    next_set = (getPreSetU(A, B, cur_set, U2bar) & X2bar) & cur_set;
    next_set = makeProper(next_set);

    if next_set == cur_set
        fprintf(" \nConverged in %d iterations\n", i);
        CIsets{2,1} = cur_set;
        break;
    end

    cur_set = next_set;
    i
    i = i + 1;
end

assert(~CIsets{2,1}.isEmptySet())


%% plot CIsets
figure
plot(X1bar, 'alpha', 1 , 'color', 'y');
hold on
plot(CIsets{1,1}, 'alpha', 0.5 , 'color', 'r');
xlabel('z')
ylabel('v_1')
zlabel('v_2')
legend('$\bar{X}_1$','$CI^\infty_1$','Interpreter','latex')
% print('', '-dpng', '-r400')

figure
plot(X2bar, 'alpha', 1 , 'color', 'c');
hold on
plot(CIsets{2,1}, 'alpha', 0.5 , 'color', 'b');
xlabel('z')
ylabel('v_1')
zlabel('v_2')
legend('$\bar{X}_2$','$CI^\infty_2$','Interpreter','latex')
% print('', '-dpng', '-r400')

%% MPC horizon
N1 = 4;
N2 = 3;
N = [N1 N2];

%% Finding nStepPresets of each CI set to get X\bar_N and then sum it with S to get initial condition constraints
nStepPreSetsCI = cell(2,1);
for i = 1:size(CIsets, 1)
    pre_set_temp_N = CIsets{i,1};
    for j = 1:N(i)
        pre_set_temp_N = getPreSetU(A, B, pre_set_temp_N, sets{2, i}) & sets{1, i};
        pre_set_temp_N = makeProper(pre_set_temp_N);
    end
    nStepPreSetsCI{i,1} = pre_set_temp_N;
end
clear pre_set_temp_N

%% Plotting n-step presets of MaxCIsets

figure
plot(CIsets{1,1}, 'alpha', 1 , 'color', 'r');
hold on
plot(nStepPreSetsCI{1,1}, 'alpha', 0.5 , 'color', 'y');
xlabel('z')
ylabel('v_1')
zlabel('v_2')
legend('$CI^\infty_1$','Pre$_1^N(CI^\infty_1)$','Interpreter','latex')
% print('dstepPreset', '-dpng', '-r400')


figure
plot(CIsets{2,1}, 'alpha', 1 , 'color', 'b');
hold on
plot(nStepPreSetsCI{2,1}, 'alpha', 0.5 , 'color', 'c');
xlabel('z')
ylabel('v_1')
zlabel('v_2')
legend('$CI^\infty_2$','Pre$_2^N(CI^\infty_2)$','Interpreter','latex')
% print('dstepPreset', '-dpng', '-r400')



%% Finding the d-step presets of the MaxCSIsets (to choose initial conditions)
% if d > N
% Doing each one in the corresponding e coordinates
dStepPreSetsCSI = cell(2,1);
for i = 1:size(CSIinf, 1)
    pre_set_temp_d = CSIinf(i,1);
    for j = 1:d(i)
        pre_set_temp_d = getPreSetU(A, B, pre_set_temp_d, sets{2, i}) & sets{1, i};
        pre_set_temp_d = makeProper(pre_set_temp_d);
    end
    dStepPreSetsCSI{i,1} = pre_set_temp_d;
end
clear pre_set_temp_d

%% Plotting d-step presets of MaxCSIsets
% Ideally I should find dStepPreSet + S but it takes too much time
% to compute. So I just plot dStepPreSets and I can choose initial
% conditions beyond the boundaries but inside + S

figure
plot(CSIinf(1,1), 'alpha', 1 , 'color', 'r');
hold on
plot(dStepPreSetsCSI{1,1}, 'alpha', 0.5 , 'color', 'y');
xlabel('z')
ylabel('v_1')
zlabel('v_2')
legend('$CSI^\infty_1$','Pre$_1^d(CSI^\infty_1)$','Interpreter','latex')
% print('dstepPreset', '-dpng', '-r400')


figure
plot(CSIinf(2,1), 'alpha', 1 , 'color', 'b');
hold on
plot(dStepPreSetsCSI{2,1}, 'alpha', 0.5 , 'color', 'c');
xlabel('z')
ylabel('v_1')
zlabel('v_2')
legend('$CSI^\infty_2$','Pre$_2^d(CSI^\infty_2)$','Interpreter','latex')
% print('dstepPreset', '-dpng', '-r400')

%% Saving workspace
save setOps.mat
