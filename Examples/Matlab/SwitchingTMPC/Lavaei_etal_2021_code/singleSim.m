clc
clear
close all

load singleModeControllerBuilt.mat

%% Simulation
% xInit = [2 -0.02 0.45]';
xInit = [1 -0.05 0.5]';
mode = 2;

% feasible initial conditions of mode 2
F2 = nStepPreSetsCI{mode,1} + S;
assert(min(F2.A*xInit <= F2.b)>=1) 

T = 80; % number of simulation steps
Xs = zeros(nx,T); % Initializing the vector of all states
Xs(:,1) = xInit;

Ux = zeros(nu,T-1); % Initializing the vector of all control inputs
UxStar = zeros(nu,T-1); % Initializing the vector of all control inputs for the nominal system

xStar0 = zeros(nx,T); % Initializing the vector of all nominal states

I = 0; % infeasiblity flag

tic
for i = 1:T
    answer = controllersSingle{mode}(Xs(:,i));
    UxStar(:,i) = answer(1:nu);
    xStar0(:,i) = answer(nu+1:end);
    
    if isnan(UxStar(:,i))
        I = i;
        fprintf('infeasible at i = %d\n',i);
        iEnd = i - 1;
        Xinfeas = Xs(:,1:iEnd);
        break;
%         I = 1;
%         break;
    end
    
    Ux(:,i) = UxStar(:,i) + K*(Xs(:,i)-xStar0(:,i));
    
    w = 2*0.005*rand(3,1) -0.005*ones(3,1);
%     w = zeros(3,1);
    Xs(:,i+1) = A*Xs(:,i) + B*Ux(:,i) + w;
end
toc

if I ~= 0
    T = I;
end

%% Plot the results
figure
stairs(1:T,Xs(1,1:T))
title('single mode')
xlabel('k')
ylabel('z')
hold on
for j = 1:T
    if mode == 2
        plot(j,Xs(1,j),'g.')
    else
        plot(j,Xs(1,j),'r.')
    end
end

stairs(1:T,xStar0(1,1:T),'k')

figure
stairs(1:T,UxStar(1,1:T))