clc
clear
close all

load setOps.mat

%Cost funtion weights
q1 = 0.2; %Cost of distance error
q2 = 0.1;  %Cost of velocity error
R = 1*[1 0; 0 1]; %Cost of accelerating

nx = size(A,1); % Number of states
nu = size(B,2); % Number of inputs

%% Building controllers of each mode in x
    controllers = cell(1,2); % one controller for each mode
    controllers{1} = cell(1,d(1));
    controllers{2} = cell(1,d(2));
    
for i = 1:size(CIsets, 1)
    yalmip('clear')
    
    ubar = sdpvar(nu*ones(1,N(i)),ones(1,N(i))); % nominal control vector
    xbar = sdpvar(nx*ones(1,N(i)+1),ones(1,N(i)+1)); % nominal state vector
    xInput = sdpvar(nx,1); % state of system at any time instant i
    
    ops = sdpsettings('verbose', 0, 'CACHESOLVERS', 1);
    
    % i = 1 => High speed
    % i = 2 => Low Speed
    
    zr = zRef(i);
    vr = vRef(i);
    
    for ts = 0:d(i)-1
        
        % at k = 1
        constraints = [(S.A)*(xInput - xbar{1})<=S.b, ... (sets{1,i}.A)*xbar{1}<=sets{1,i}.b,
            (sets{2,i}.A)*ubar{1}<=sets{2,i}.b, xbar{2} == A*xbar{1}+B*ubar{1}];
        objective = q1*(xbar{1}(1)-zr)^2/(xbar{1}(1)-vr)...
            +q2*((xbar{1}(2)-vr)^2+(xbar{1}(3)-vr)^2)...
            +ubar{1}'*R*ubar{1};
        
        % adding running cost and constraints
        for k=2:N(i)
            objective = objective+q1*(xbar{k}(1)-zr)^2/(xbar{k}(1)-vr)...
                +q2*((xbar{k}(2)-vr)^2+(xbar{k}(3)-vr)^2)...
                +ubar{k}'*R*ubar{k};
            constraints = [constraints; xbar{k+1}==A*xbar{k}+B*ubar{k},... %Dynamic Constraint
                sets{2,i}.A*ubar{k}<=sets{2,i}.b];  %Input Constraint
            if k < d(i)-ts+1
                constraints = [constraints; sets{1,i}.A*xbar{k}<=sets{1,i}.b]; %State Constraint
            else % k >= d(i)-ts+1
                constraints = [constraints; CSIinf(i,1).A*xbar{k}<=CSIinf(i,1).b]; %State Constraint
            end
        end
        
        % adding terminal objective
        objective = objective + q1*(xbar{N(i)+1}(1)-zr)^2/(xbar{N(i)+1}(1)-vr)...
            +q2*((xbar{N(i)+1}(2)-vr)^2+(xbar{N(i)+1}(3)-vr)^2);
        
        
        % adding terminal constraints
        if d(i)-ts-N(i) > 0
            temp = d(i)-ts-N(i);
            setTemp = CSIinf(i,1);
            for j = 1:temp
                setTemp = getPreSetU(A, B, setTemp, sets{2, i}) & sets{1, i};
                setTemp = makeProper(setTemp);
            end
            constraints = [constraints;setTemp.A*xbar{N(i)+1} <= setTemp.b];
        else
            constraints = [constraints; CSIinf(i,1).A*xbar{N(i)+1} <= CSIinf(i,1).b];
        end
        
        controllers{i}{ts+1} = optimizer(constraints,objective,ops,xInput,[ubar{1};xbar{1}]);
        clear constraints
    end
end

save swControllersBuilt.mat
