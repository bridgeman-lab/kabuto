clc
clear
close all

load singleModeControllerBuilt.mat

%% Simulation
xInit = [1 -0.04 0.5]';

T = 100; % number of simulation steps
mode = zeros(1,T); % vector of modes
mode(1) = 2;

% feasible initial conditions of mode 2
F2 = dStepPreSetsCSI{mode(1),1} + S;
assert(min(F2.A*xInit <= F2.b)>=1)

Xs = zeros(nx,T); % Initializing the vector of all states
Xs(:,1) = xInit;

Ux = zeros(nu,T-1); % Initializing the vector of all control inputs
UxStar = zeros(nu,T-1); % Initializing the vector of all control inputs for the nominal system

xStar0 = zeros(nx,T); % Initializing the vector of all nominal states

w = zeros(nx,T);


ts = 0; % time since last switch

I = 0; % infeasiblity flag


tic
for i = 1:T
    
    
    if ts >= d(2) && mode(i-1) == 2 && Xs(1,i) > Xs(1,i-1)
        disp("sw")
        mode(i) = 1;
        ts = 0;
    elseif ts >= d(1) && mode(i-1) == 1 && Xs(1,i) < Xs(1,i-1)
        disp("sw")
        mode(i) = 2;
        ts = 0;
    elseif i > 1
        mode(i) = mode(i-1);
    end
    
%     if i >= 63
%         mode(i) = 1;
%     end
    
    answer = controllersSingle{mode(i)}(Xs(:,i));
    UxStar(:,i) = answer(1:nu);
    xStar0(:,i) = answer(nu+1:end);
    
    if isnan(UxStar(1,i))
        I = i;
        fprintf('infeasible at i = %d\n',i);
        iEnd = i - 1;
        Xinfeas = Xs(:,1:iEnd);
        break;
    end
    
    Ux(:,i) = UxStar(:,i) + K*(Xs(:,i)-xStar0(:,i));
    
    w(:,i) = 2*0.005*rand(3,1) - 0.005*ones(3,1);
    
    Xs(:,i+1) = A*Xs(:,i) + B*Ux(:,i) + w(:,i);
    
    ts = ts + 1;
    
end
toc

if I ~= 0
    T = I;
end

%% Plot the results
figure
stairs(1:T,Xs(1,1:T))
title('unconstrained switches')
xlabel('k')
ylabel('z')
hold on
for j = 1:T
    if mode(j) == 2
        plot(j,Xs(1,j),'g.')
    else
        plot(j,Xs(1,j),'r.')
    end
end

stairs(1:T,xStar0(1,1:T),'k')
% save mySwitchingSequence.mat