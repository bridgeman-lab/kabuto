/* 
 * set LEDs using class system
 * eem41
 */

#include <ros.h>
#include <ros/time.h>
#include <std_msgs/Int8.h>

//creates Arduino as new ROS NODE
ros::NodeHandle  nh;

void cbmsg6( const std_msgs::Int8& ledmsg6){
  int lit6 = ledmsg6.data;
  if(lit6 == 0) {
    digitalWrite(6, LOW);
  }
  else{
    digitalWrite(6, HIGH);
  }
}
void cbmsg7( const std_msgs::Int8& ledmsg7){
  int lit7 = ledmsg7.data;
  if(lit7 == 0) {
    digitalWrite(7, LOW);
  }
  else{
    digitalWrite(7, HIGH);
  }
}

//subscribe to topics
ros::Subscriber<std_msgs::Int8> led6("/robot_0/digital_output6", &cbmsg6);
ros::Subscriber<std_msgs::Int8> led7("/robot_0/digital_output7", &cbmsg7);

void setup() {
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  nh.initNode();
  nh.subscribe(led6);
  nh.subscribe(led7);
}

void loop() {
  nh.spinOnce();
  delay(1);
}
