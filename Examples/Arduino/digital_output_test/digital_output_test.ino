/* 
 * set LEDs using class system
 * eem41
 */

#include <ros.h>
#include <ros/time.h>
#include <std_msgs/UInt8.h>

//creates Arduino as new ROS NODE
ros::NodeHandle  nh;

void cbLed( const std_msgs::UInt8& ledmsg){
  int led_msg = ledmsg.data;
  const uint8_t gpio_num_mask = 0b01111111;
  uint8_t gpio_num = led_msg & gpio_num_mask;
  const bool is_on = 0b10000000 & led_msg;
  
  pinMode(gpio_num, OUTPUT);
  digitalWrite(gpio_num, is_on);
}

//subscribe to topics
ros::Subscriber<std_msgs::UInt8> led("/robot_0/digital_output", &cbLed);


void setup() {
  nh.initNode();
  nh.subscribe(led);
}

void loop() {
  nh.spinOnce();
  delay(1);
}
