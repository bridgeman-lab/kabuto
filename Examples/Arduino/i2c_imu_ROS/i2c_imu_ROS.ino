#define USE_USBCON
#include <ros.h>
#include <ros/time.h>
#include <std_msgs/Float32MultiArray.h>
#include <Adafruit_BNO08x.h>


#define BNO08X_RESET -1

#define ROBOT_NAME "robot_1" /** <=== Change name here to match robot getting flashed to */

Adafruit_BNO08x  bno08x(BNO08X_RESET);
sh2_SensorValue_t sensorValue;

/** Global Variable to track rotation vector in the quaternion form: {real, i, j, k}. */
float imu_values [4] = {0.0, 0.0, 0.0, 0.0};

/** ROS handle to attach arduino over ROS serial */
ros::NodeHandle ros_node_handle;

/** Publish IMU data */
std_msgs::Float32MultiArray rotation_vector;
ros::Publisher imu_pub("/" ROBOT_NAME "/IMU", &rotation_vector);

// FUNCTION PROTOTYPES

/** Define type of Adafruit Sensor. */
void setIMU();
/** Read data from IMU. */
void readIMU();
/** Update publishers and subscribers and spin ros */
void spinROS();


void setup() {
  setIMU();
  ros_node_handle.initNode();
  ros_node_handle.advertise(imu_pub); //Advertise ROS node
}

void loop() {
  readIMU();
  spinROS();
  delay(10);
}

void setIMU(){
  bno08x.begin_I2C(); //Begin I2C
  bno08x.enableReport(SH2_GAME_ROTATION_VECTOR); // Define the sensor outputs you want to receive -- full list located in sh2.h line 85
  delay(1000);
}

void readIMU(){
  bno08x.getSensorEvent(&sensorValue);
  imu_values[0] = sensorValue.un.rotationVector.real;
  imu_values[1] = sensorValue.un.rotationVector.i;
  imu_values[2] = sensorValue.un.rotationVector.j;
  imu_values[3] = sensorValue.un.rotationVector.k;

  rotation_vector.data = imu_values;
  rotation_vector.data_length = 4;
}

void spinROS(){
  imu_pub.publish(&rotation_vector);
  ros_node_handle.spinOnce();
}