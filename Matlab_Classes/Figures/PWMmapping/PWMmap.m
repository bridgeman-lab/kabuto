close all; clear all; clc;
rosshutdown
ipaddress = "http://192.168.2.16:11311";
setenv('ROS_MASTER_URI', ipaddress);
rosinit(ipaddress);

robot_0 = Kabuto('robot_0');   

dwell_time = 2; %s
pause(2);

int = 40;

%pwm=[0 10 20 30 40 50 60 70 80 90 100 110 120 130 140 150 160 170 180 190 200 210 220 230 240 250];
pwm=[-250 -240 -230 -220 -210 -200 -190 -180 -170 -160 -150 -140 -130 -120 -110 -100 -90 -80 -70 -60 -50 -40 -30 -20 -10 0 ...
    10 20 30 40 50 60 70 80 90 100 110 120 130 140 150 160 170 180 190 200 210 220 230 240 250];

speed_left_hist = zeros(1,int);
speed_right_hist = zeros(1,int);
t = linspace(0, 51*int/10,51*int);

speed_left_pwm = zeros(1, 20);
speed_right_pwm = zeros(1, 20);
avg_speed_left = zeros(1, 51);
avg_speed_right = zeros(1, 51);

k=1;
for l = 1:length(pwm)
    robot_0.right_wheel.writePWM(pwm(l));
    robot_0.left_wheel.writePWM(pwm(l));
    figure(l); hold on; % Kp = 1, Ki = 0.0003, Kd = 0
    while k<(int+1)
        speed_left = robot_0.left_wheel.readSpeed();
        speed_left_hist(k)=speed_left;
        speed_right = robot_0.right_wheel.readSpeed();
        speed_right_hist(k)=speed_right;
        plot(t(1:k), speed_left_hist(1:k), 'r', 'Linewidth', 1); hold on;
        plot(t(1:k), speed_right_hist(1:k), 'b', 'Linewidth', 1);
        drawnow
        pause(.1);
        if k>20
            speed_left_pwm(k-20) = speed_left_hist(k);
            speed_right_pwm(k-20) = speed_right_hist(k);
        end

        k=k+1;
    end
    avg_speed_left(l) = mean(speed_left_pwm);
    avg_speed_right(l) = mean(speed_right_pwm);

    k=1;
    pwmstr = num2str(pwm(l));
    pwmtitle = strcat('PWM_',pwmstr);
    title(['PWM = ' pwmstr]);
    legend({'Left Wheel', 'Right Wheel'}, 'Location', 'northeast');
    xlim([0 4]);
%     ylim([-650 0]);
    xlabel('Time (s)')
    ylabel('Velocity (mm/s)');
    saveas(gcf, pwmtitle, 'png');

    robot_0.right_wheel.writeVel(0.0);
    robot_0.left_wheel.writeVel(0.0);
    robot_0.right_wheel.writePWM(0);
    robot_0.left_wheel.writePWM(0);
    pause(5);
    
    disp(pwm(l));
end

figure(l+1)
plot(pwm,avg_speed_left, 'r'); hold on;
plot(pwm,avg_speed_right, 'b');
title('PWM vs. Avg. Velocity,Kabuto0');
legend({'left wheel', 'right wheel'}, 'Location', 'northwest');
xlabel('PWM Val');
ylabel('Velocity (mm/s)');
saveas(gcf, 'PWMmap_kabuto0.png');





