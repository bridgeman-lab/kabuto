classdef KabutoEncoderInput < handle
    %KABUTOENCODERINPUT - Creates a Kabuto encoder input object to
    %handle receiving of encoder and velocity data.
    %   This class handles the receiving of encoder data and low-level
    %   velocity controller data. Within this class are two functions that
    %   allow the user to call to obtain raw encoder data and calculated
    %   velocity data that is computed by the Arduino firmware
    %   (Arduino_Firmware.ino).
    %
    %   KabutoEncoderInput methods:
    %       readPos                 - Returns int32 with current count of encoder
    %       readSpeed               - Returns Float64 with velocity in mm/s
    %
    %   KabutoEncoderInput properties:
    %       topic_name_raw          - Stores rostopic name for raw encoder data
    %       topic_name_vel          - Stores rostopic name for encoder velocity data
    %       id                      - Stores name of wheel encoder (right_wheel or left_wheel)
    %       encoder_sub             - Creates subscriber to the raw encoder data topic
    %       encoder_vel_sub         - Creates subscriber to the calculated velocity topic
    
    properties
        topic_name_raw
        topic_name_vel
        id
        encoder_sub
        encoder_vel_sub
    end

    methods
        function obj = KabutoEncoderInput(device_name, encoder_id)
            %KABUTOENCODERINPUT - Construct an instance of this class
            %   Creates and sets up encoder and velocity data topic names
            %   and subscribers
            obj.id = encoder_id;
            obj.topic_name_raw = string(['/',device_name,'/',encoder_id,'/encoder_raw']);
            obj.encoder_sub = rossubscriber(obj.topic_name_raw, 'std_msgs/Int32', 'DataFormat','struct');
            obj.topic_name_vel = string(['/',device_name,'/',encoder_id,'/encoder_vel']);
            obj.encoder_vel_sub = rossubscriber(obj.topic_name_vel, 'std_msgs/Float64', 'DataFormat','struct');
        end

        function value = readPos(obj)
            %READPOS - Returns int32 with current count of encoder
            %ticks
            %   Receives a std_msgs/Int32 type ROS message 
            %   containing encoder data on the topic specified by the
            %   object id and device name

            pos = obj.encoder_sub.LatestMessage;
            value = pos.Data;
        end

        function vel = readSpeed(obj)
            %READSPEED - Returns Float64 with velocity in mm/s
            %   Receives a std_msgs/Float64 type ROS message 
            %   containing velocity data on the topic specified by the
            %   object id and device name. Velocity is calculated in
            %   Arduino firmware through the use of a buffer to sample and
            %   derivate over time.

            speed = obj.encoder_vel_sub.LatestMessage;
            vel = speed.Data;
        end
    end
end