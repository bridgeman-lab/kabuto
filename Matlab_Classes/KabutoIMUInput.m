classdef KabutoIMUInput
    %KABUTOIMUINPUT - Creates a Kabuto IMU input object to
    %handle receiving of quaternion vector.
    %   This class handles the receiving of encoder data in Game Rotation quaternion form.
    %   Data is returned as an array of the form [r, i, j, k].
    %
    %   KabutoIMUInput methods:
    %       readIMU                 - Returns float32 array with
    %                                   instantaneous IMU quaternion data
    %
    %   KabutoIMUInput properties:
    %       topic_name              - Stores rostopic name for IMU data
    %       IMU_sub                 - Creates subscriber to the IMU data topic

    properties
        topic_name
        IMU_sub
    end

    methods
        function obj = KabutoIMUInput(device_name)
            %KabutoIMUInput Construct an instance of this class
            %   Sets up subsciber node to the IMU topic of a specific robot
            obj.topic_name = string(['/',device_name,'/IMU']);
            obj.IMU_sub = rossubscriber(obj.topic_name, 'std_msgs/Float32MultiArray', 'DataFormat','struct');
        end

        function data = readIMU(obj)
            %READSPEED - Returns float32 array with in quaternion form ([r,i,j,k])
            %   Receives a std_msgs/Float32Array type ROS message 
            %   containing IMU data on the topic specified by the
            %   object id and device name. Quaternion data is calculated in
            %   Arduino firmware through the use of a built in sensor fusion
            %   algorithm written by Adafruit (Game Rotation Vector). 
            %   More documentation can be found here: 
            %   https://learn.adafruit.com/adafruit-9-dof-orientation-imu-fusion-breakout-bno085/report-types

            IMU_data = obj.IMU_sub.LatestMessage;
            data = IMU_data.Data;
        end
    end
end