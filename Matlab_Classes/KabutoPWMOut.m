classdef KabutoPWMOut
    %KABUTOPWMOUT Send PWM message to a motor side on the robot
    %   obj = KabutoPWMOut() creates a ROS topic and publisher to send PWM
    %   value to a motor
    %
    %   KabutoPWMOut methods:
    %       setPWM                  - Creates a ROS message containing a PWM value
    %
    %   KabutoPWMOut properties:
    %       topic_name              - Stores rostopic name to send PWM value on
    %       pwmpub                  - Creates publisher to the PWM topic
    %       pwmmsg                  - Store ROS message of type Int16

    properties
        topic_name
        pwmpub
        pwmmsg
    end

    methods
        function obj = KabutoPWMOut(device_name, object_id)
            %KabutoPWMOut Construct an instance of this class
            %   Creates a ROS topic and publisher to send PWM value to a motor
            obj.topic_name = string(['/',device_name,'/',object_id,'/pwm_input']);
            obj.pwmpub = rospublisher(obj.topic_name, 'std_msgs/Int16', 'DataFormat','struct');

        end

        function setPWM(obj, val)
            %setPWM - Creates a ROS message containing a PWM value
            %   Valid inputs are integers from -255 to 255
            obj.pwmmsg = rosmessage(obj.pwmpub);
            obj.pwmmsg.Data = int16(val); %converts to int16 to send over ROS
            
            send(obj.pwmpub,obj.pwmmsg);
        end
    end
end