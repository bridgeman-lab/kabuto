classdef Kabuto
    %KABUTO - Class creates a Kabuto robot object.
    %   obj = Kabuto() creates subclasses that contain different parts of
    %   the robot, including gpio pin access, wheel management, and sensor
    %   suite.
    %
    %   Kabuto methods:
    %       setLEDS                 - Digital write HIGH or LOW to gpio pins on Arduino
    %       readEncoders            - Returns 1x2 struct containing encoder data from right and left wheels
    %
    %   Kabuto properties:
    %       led_0                   - Instantiates subclass KabutoDigitalOutput
    %       led_1                   - Instantiates subclass KabutoDigitalOutput
    %       right_wheel             - Instantiates subclass KabutoWheelPack
    %       left_wheel              - Instantiates subclass KabutoWheelPack
    
    properties
        led_0
        led_1
        right_wheel KabutoWheelPack
        left_wheel  KabutoWheelPack
        IMU KabutoIMUInput
    end

    methods
        function obj = Kabuto(device_name)
            %KABUTO - Construct an instance of this class
            %   Sets up objects within the robot
            obj.led_0 = KabutoDigitalOut(device_name, 6);
            obj.led_1 = KabutoDigitalOut(device_name, 8);

            obj.left_wheel  = KabutoWheelPack(device_name,'left_wheel');
            obj.right_wheel = KabutoWheelPack(device_name,'right_wheel');

            obj.IMU = KabutoIMUInput(device_name);
        end

        function setLEDs(obj,values)
            %SETLEDS - Digital write HIGH or LOW to gpio pins on Arduino
            %   Valid inputs for values are 0 or 1, where 0(LOW) and
            %   1(HIGH). You can add more led properties in order to write
            %   more pins high or low. LEDs were used as a common, viewable
            %   example.
            obj.led_0.set(values(1));
            obj.led_1.set(values(2));
        end

        function [enc_val_left, enc_val_right] = readEncoders(obj)
            %READENCODERS - Returns 1x2 struct containing encoder data from
            %right and left wheel
            %   Returns 1x2 struct of Int32 data in the format
            %   [wheel_left_encoder_data, wheel_right_encoder_data]
            
            enc_val_left = obj.left_wheel.encoder.read();
            enc_val_right = obj.right_wheel.encoder.read();
        end
    end
end