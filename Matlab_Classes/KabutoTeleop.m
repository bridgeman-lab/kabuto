rosshutdown
ipaddress = "http://192.168.2.16:11311";
setenv('ROS_MASTER_URI', ipaddress);
rosinit(ipaddress);

robot_0 = Kabuto('robot_0');
% robot_1 = Kabuto('robot_1');




k=0;

keyObj = KeyInput();
reply = 0;
    
    while reply~='q'
        pwm_right = 0;   % Initialize linear and angular velocities
        pwm_left = 0;      
        reply = getKeystroke(keyObj);
        switch reply
            case 'i'     % i
                pwm_right = 100; %change pwm_right and pwm_left to input new things
                pwm_left = 100;
            case 'k'     % k
                pwm_right = -100;
                pwm_left = -100;
            case 'j'     % j
                pwm_right = 100;
                pwm_left = -100;
            case 'l'     % l
                pwm_right = -100;
                pwm_left = 100;
        end
        robot_0.right_wheel.writePWM(pwm_right);
        robot_0.left_wheel.writePWM(pwm_left);
    end

poses = handles.poses;
closeFigure(keyObj);

% while(true)
% 
% %     robot_0.right_wheel.writePWM(100);
% %     rostopic echo /robot_0/right_wheel/encoder_vel
% 
% %     encoderval = robot_0.wheel_right.getEncoder();
% %     pause(dwell_time);
% %     disp(encoderval)
% 
% %     [l,r] = robot_0.readEncoders();q
% %     disp([l,r]);
% %     pause(dwell_time);
% 
% 
%     for pwm_ramp = [0, 50, 100, 200, 100, 50, 0, -50, -100, -200, -100, -50]
%         robot_0.right_wheel.writePWM(pwm_ramp);
%         fprintf('\npwm is: %3.0f, current_speed is: ', pwm_ramp);
%         pause(0.5);
%         current_speed = robot_0.right_wheel.readSpeed();
%         fprintf('%10f mm/s\n',current_speed);
%         pause(.25);
% %         pause(dwell_time);
% 
%     end

% %     speed = robot_0.right_wheel.readSpeed();
% %     pause(.1);
% %     disp(speed);
% 
% %     k=k+1;
% %     plot(k,speed,'k-');
% %     drawnow
% 
% %     robot_0.setLEDs([1,0]);
% %     robot_1.setLEDs([0,0]);
% %     pause(dwell_time);
% %     robot_0.setLEDs([1,1]);
% %     robot_1.setLEDs([0,0]);
% %     pause(dwell_time);
% %     robot_0.setLEDs([1,1]);
% %     robot_1.setLEDs([1,0]);
% %     pause(dwell_time);
% %     robot_0.setLEDs([0,1]);
% %     robot_1.setLEDs([1,1]);
% %     pause(dwell_time);
% %     robot_0.setLEDs([0,0]);
% %     robot_1.setLEDs([1,1]);
% %     pause(dwell_time);
% %     robot_0.setLEDs([0,0]);
% %     robot_1.setLEDs([0,1]);
% %     pause(dwell_time);
% %     robot_0.setLEDs([0,0]);
% %     robot_1.setLEDs([0,0]);
% %     pause(dwell_time);
% end