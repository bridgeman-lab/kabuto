classdef KabutoDigitalOut < handle
    %KABUTODIGITALOUT - Class for remotely controlling the digital output
    %of a pin on Arduino
    %   obj = KabutoDigitalOut() creates a ROS topic and publisher to a GPIO pin on the Arduino.
    %
    %   KabutoDigitalOut methods:
    %       set                     - Creates a UInt ROS message can be filtered using a bitmask -- used in conjuction with turnOff() and turnOn()
    %       turnOff                 - Sets a GPIO pin low in Arduino
    %       turnOn                  - Sets a GPIO pin high in Arduino
    %
    %   KabutoDigitalOut properties:
    %       topic_name              - Stores rostopic name of Obj + GPIO pin
    %       gpio_num                - Stores pin number of desired GPIO 
    %       ledpub                  - Creates publisher to the GPIO topic
    %       ledmsg                  - Store ROS message of type Int8

    properties
        topic_name
        gpio_num
        ledpub
        ledmsg
    end

    methods
        function obj = KabutoDigitalOut(device_name, gpio_num)
            %KABUTODIGITALOUT - creates a ROS topic and publisher to a GPIO
            %pin on the Arduino
            obj.gpio_num = gpio_num;
            obj.topic_name = string(['/',device_name,'/digital_output']);
            obj.ledpub = rospublisher(obj.topic_name, 'std_msgs/UInt8', 'DataFormat','struct');
            
        end

        function set(obj, val)
            %SET - Creates a UInt ROS message can be filtered using a bitmask -- used in conjuction with turnOff() and turnOn()
            obj.ledmsg = rosmessage(obj.ledpub);
            obj.ledmsg.Data = uint8((val*128)+obj.gpio_num); %converts to int8 to send over ROS
            
            send(obj.ledpub,obj.ledmsg);
        end

        function turnOff(obj)
            %TURNOFF - Sets a GPIO pin low in Arduino
            obj.set(0);
        end

        function turnOn(obj)
            %TURNON - Sets a GPIO pin high in Arduino
            obj.set(1);
        end
    end
end