classdef KabutoVelOut
    %KABUTOVELOUT Send desired velocity (mm/s) message to a motor side on the robot
    %   obj = KabutoVelOut() creates a ROS topic and publisher to send
    %   desired velocity
    %
    %   KabutoVelOut methods:
    %       setVel                  - Creates a ROS message containing a float value that includes the desired velocity in mm/s
    %
    %   KabutoPWMOut properties:
    %       topic_name              - Stores rostopic name to send velocity value on
    %       velpub                  - Creates publisher to the vel_input topic
    %       velmsg                  - Store ROS message of type Float64

    properties
        topic_name
        velpub
        velmsg
    end

    methods
        function obj = KabutoVelOut(device_name, object_id)
            %KABUTOVELOUT - Construct an instance of this class
            %   Creates a ROS topic and publisher to send desired velocity
            obj.topic_name = string(['/',device_name,'/',object_id,'/vel_input']);
            obj.velpub = rospublisher(obj.topic_name, 'std_msgs/Float64', 'DataFormat','struct');

        end

        function setVel(obj, val)
            %SETVEL - Creates a ROS message containing a float value that includes the desired velocity in mm/s
            %   Sends desired velocity over ROS
            %   Valid inputs are floats from -700.0 to 700.0.
            obj.velmsg = rosmessage(obj.velpub);
            obj.velmsg.Data = double(val); %converts to Float64 to send over ROS
            
            send(obj.velpub,obj.velmsg);
        end
    end
end