classdef KabutoWheelPack < handle
    %KABUTOWHEELPACK - Class for creating a robot motor wheel object
    %   obj = KabutoWheelPack() includes functionality for writing PWM
    %   signals to the motor or desired velocity to the motor, as well as
    %   receiving data for measured velocity of the robot's wheel.
    %
    %   KabutoWheelPack methods:
    %       writePWM                - Sends PWM command over ROS
    %       writeVel                - Sends desired velocity over ROS 
    %       readSpeed               - Receives measured velocity of wheel over ROS
    %
    %   KabutoWheelPack properties:
    %       encoder                 - Instantiates subclass KabutoEncoderInput
    %       motor                   - Instantiates subclass KabutoPWMOut
    %       PID                     - Instantiates subclass KabutoVelOut

    properties
        encoder KabutoEncoderInput
        motor   KabutoPWMOut
        PID     KabutoVelOut
    end

    methods
        function obj = KabutoWheelPack(device_name, wheel_pack_name)
            %KABUTOWHEELPACK - Construct an instance of this class
            %   Creates objects of subclasses within this class

            obj.motor = KabutoPWMOut(device_name, wheel_pack_name);
            obj.PID = KabutoVelOut(device_name, wheel_pack_name);
            obj.encoder = KabutoEncoderInput(device_name, wheel_pack_name);
        end

        function writePWM(obj, val)
            %WRITEPWM - Sends PWM command over ROS
            %   Valid inputs are integers from -255 to 255 to send a raw PWM
            %   signal to the wheel described with this class. Using this
            %   function will disable the low-level velocity controller on
            %   the Arduino,
            obj.motor.setPWM(val);
        end


        function writeVel(obj, val)
            %WRITEVEL - Sends desired velocity over ROS
            %   Valid inputs are floats from -700.0 to 700.0. An onboard
            %   low-level controller will handle the speed control if you
            %   do not want to write your own.
            obj.PID.setVel(val);
        end


        function w = readSpeed(obj)
            %READSPEED - Receives measured velocity of wheel over ROS
            %   Detailed explanation goes here
            w = obj.encoder.readSpeed();
        end
    end
end