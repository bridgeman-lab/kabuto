/* 
 * Arduino ROSserial PWM control w/ Encoders and disableable PID controller
 * Authored by Emi Muranaka (eem41) w/ help from and advisement from Richard Hall (rah75)
 * Derived from https://github.com/JeelChatrola/Control-of-motor-with-ROS
 */


#define USE_USBCON // This line is necessary in order for ROSserial to recognize the Arduino Due as a ROS node. See https://github.com/ros-drivers/rosserial/issues/284#issuecomment-312502805 for more info.

// Include robot's configuration file here
#include "kabuto0.h"

#include <ros.h>
#include <ros/time.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Float32MultiArray.h>
#include <Encoder.h>
#include <ArduPID.h>
#include <Adafruit_BNO08x.h>

// Macros for building message string names
#define LEFT_WHEEL_MSG(S) "/" ROBOT_NAME "/left_wheel/" S
#define RIGHT_WHEEL_MSG(S) "/" ROBOT_NAME "/right_wheel/" S

// arduino hardware pins
#define PIN_MOTOR_L_DIR_1     9
#define PIN_MOTOR_L_DIR_2     7
#define PIN_MOTOR_L_ENA       6
#define PIN_MOTOR_R_DIR_1     11
#define PIN_MOTOR_R_DIR_2     10
#define PIN_MOTOR_R_ENB       12
#define PIN_ENCODER_L_1       24
#define PIN_ENCODER_L_2       25
#define PIN_ENCODER_R_1       22
#define PIN_ENCODER_R_2       23
#define VELOCITY_CONSTANT     85300 // Converts ticks/us to mm/s
#define VELOCITY_BUFFER_SIZE  10

#define BNO08X_RESET -1 // Reset IMU

/*=============================================================================
  |                            Global Variables                               |
  =============================================================================*/
typedef enum {SIDE_L = 0, SIDE_R = 1} side_t;

/** True of velocity msg was recieved after latest pwm msg. Else false. */
bool velocity_set = false;
/** Values to write to the motors. Either from PIDs or PWM msg depending on velocity_set. */
int16_t pwm_vals [2] = {0,0};
/** Values to write to the motors. Either from PIDs or PWM msg depending on velocity_set. */
double pwm_vals_double [2] = {0.0,0.0};
/** The target velocity for both wheels. Used by PID controllers as it's setpoint. */
double vel_desired [2] = {0.0,0.0};
/** The current velocity of the wheels. Stored as doubles for the PID controller. */
double vel_current [2] = {0.0,0.0};
/** The previous desired velocity of the wheels. Stored as doubles for the PID controller. */
double vel_desired_prev [2] = {0.0,0.0};
/** Global Variable to track rotation vector in the quaternion form: {real, i, j, k}. */
float imu_values [4] = {0.0, 0.0, 0.0, 0.0};

/** A left and right encoder object attached to each wheel */
Encoder encoder [2] = { 
  Encoder(PIN_ENCODER_L_1, PIN_ENCODER_L_2),
  Encoder(PIN_ENCODER_R_1, PIN_ENCODER_R_2)};

/** IMU object initialization*/
Adafruit_BNO08x  bno08x(BNO08X_RESET);
sh2_SensorValue_t sensorValue;

/** PID function that corrects a FF controller to regulate wheels to a set velocity (left side and right side) */ 
const double Kp=1.6, Ki=0.005, Kd=1.8;
ArduPID vel_pid [2];

/** ROS handle to attach arduino over ROS serial */
ros::NodeHandle ros_node_handle;

/*=============================================================================
  |                          Function Prototypes                              |
  =============================================================================*/
/** Read and save the encoder values and compute the motor velocity */
void computeEncoderAndVel();
/** Tick PID controllers on both wheels and read new PWM valus into pwm_vals array */
void computePWMValues(); 
/** Write the corrisponding pwm valus to both wheels, adjusting the dir pins to match the sign. */
void writeToMotor();
/** Define type of Adafruit Sensor. */
void setIMU();
/** Read data from IMU. */
void readIMU();
/** Update publishers and subscribers and spin ros */
void spinROS();

/*=============================================================================
  |                     PWM subscribers and callback                          |
  =============================================================================*/
void pwmInputR(const std_msgs::Int16& pwm_val_msg){
  pwm_vals[SIDE_R] = pwm_val_msg.data;
  velocity_set = false;
}

void pwmInputL(const std_msgs::Int16& pwm_val_msg){
  pwm_vals[SIDE_L] = pwm_val_msg.data;
  velocity_set = false;
}

ros::Subscriber<std_msgs::Int16> motor_pwm [2] = {
  ros::Subscriber<std_msgs::Int16>(LEFT_WHEEL_MSG("pwm_input"),  &pwmInputL),
  ros::Subscriber<std_msgs::Int16>(RIGHT_WHEEL_MSG("pwm_input"), &pwmInputR)};

/*=============================================================================
  |                   Velocity subscribers and callback                       |
  =============================================================================*/
void velInputL(const std_msgs::Float64& vel_msg){
  vel_desired[SIDE_L] = double(vel_msg.data);
  velocity_set = true;
}

void velInputR(const std_msgs::Float64& vel_msg){
  vel_desired[SIDE_R] = double(vel_msg.data);
  velocity_set = true;
}

ros::Subscriber<std_msgs::Float64> desired_motor_vel [2] = {
  ros::Subscriber<std_msgs::Float64>(LEFT_WHEEL_MSG("vel_input"),  &velInputL),
  ros::Subscriber<std_msgs::Float64>(RIGHT_WHEEL_MSG("vel_input"), &velInputR)};

/*=============================================================================
  |                            Encoder publishers.                            |
  =============================================================================*/
std_msgs::Int32 encoder_val [2];
ros::Publisher encoder_pub [2] = {
  ros::Publisher(LEFT_WHEEL_MSG("encoder_raw"), &encoder_val[SIDE_L]),
  ros::Publisher(RIGHT_WHEEL_MSG("encoder_raw"), &encoder_val[SIDE_R])};

/*=============================================================================
  |                           Velocity publishers                             |
  =============================================================================*/
std_msgs::Float64 motor_vel [2];
ros::Publisher motor_vel_pub [2] = {
  ros::Publisher(LEFT_WHEEL_MSG("encoder_vel"), &motor_vel[SIDE_L]),
  ros::Publisher(RIGHT_WHEEL_MSG("encoder_vel"), &motor_vel[SIDE_R])};


/*=============================================================================
  |                               IMU publisher                               |
  =============================================================================*/
std_msgs::Float32MultiArray rotation_vector;
ros::Publisher imu_pub("/" ROBOT_NAME "/IMU", &rotation_vector);


/*=============================================================================
  |                                   Setup                                   |
  =============================================================================*/
void setup(){
  // Setup for IMU
  setIMU();
  // ROS Setup
  ros_node_handle.initNode();
  for (int s = SIDE_L; s <= SIDE_R; s++){
    ros_node_handle.advertise(encoder_pub[s]);
    ros_node_handle.advertise(motor_vel_pub[s]);
    ros_node_handle.subscribe(motor_pwm[s]);
    ros_node_handle.subscribe(desired_motor_vel[s]);
  }
  ros_node_handle.advertise(imu_pub);

  // PID Setup
  for(int motor_side = 0; motor_side<2; motor_side++){
    vel_pid[motor_side].setOutputLimits(-255, 255);
    vel_pid[motor_side].setWindUpLimits(-1000, 1000);
    vel_pid[motor_side].begin(&vel_current[motor_side], &pwm_vals_double[motor_side], &vel_desired[motor_side], Kp, Ki, Kd);
    vel_pid[motor_side].start();
  }

  // Pin Setup
  pinMode(PIN_MOTOR_L_DIR_1, OUTPUT);
  pinMode(PIN_MOTOR_L_DIR_2, OUTPUT);
  pinMode(PIN_MOTOR_L_ENA,   OUTPUT);
  pinMode(PIN_MOTOR_R_DIR_1, OUTPUT);
  pinMode(PIN_MOTOR_R_DIR_2, OUTPUT);
  pinMode(PIN_MOTOR_R_ENB,   OUTPUT);
}

/*=============================================================================
  |                                 Main Loop                                 |
  =============================================================================*/
void loop(){
  uint32_t start_time = millis();
  computeEncoderAndVel();
  computePWMValues();
  writeToMotor();
  readIMU();
  spinROS();

  // Spin till 10ms have passed
  while(start_time + 10 >= millis()) {}
}

/*=============================================================================
  |                                 Functions                                 |
  =============================================================================*/
/** Read and save the encoder values and compute the motor velocity */
void computeEncoderAndVel(){
  // Variables that will persist between function calls
  static uint8_t motor_vel_buff_idx [2] = {0,0};
  static float motor_vel_buff [2][VELOCITY_BUFFER_SIZE];
  static long lastLoopTime_us = 0;

  // Read new encoder values
  const int32_t new_enc_data [2] = {encoder[SIDE_L].read(), encoder[SIDE_R].read()};
  const long delta_time = micros() - lastLoopTime_us;
  const float delta_time_f = delta_time;

  // Compute velocity and store in velocity buffers
  motor_vel_buff[SIDE_L][motor_vel_buff_idx[SIDE_L]] = VELOCITY_CONSTANT * (new_enc_data[SIDE_L] - encoder_val[SIDE_L].data)/delta_time_f;
  motor_vel_buff[SIDE_R][motor_vel_buff_idx[SIDE_R]] = VELOCITY_CONSTANT * (new_enc_data[SIDE_R] - encoder_val[SIDE_R].data)/delta_time_f;

  // Increment velocity buffer indicies
  motor_vel_buff_idx[SIDE_L] = (motor_vel_buff_idx[SIDE_L]+1)%VELOCITY_BUFFER_SIZE;
  motor_vel_buff_idx[SIDE_R] = (motor_vel_buff_idx[SIDE_R]+1)%VELOCITY_BUFFER_SIZE;

  // Store average velocities in ROS msgs and for the PID controller
  motor_vel[SIDE_L].data = 0; // mm/s
  motor_vel[SIDE_R].data = 0; 
  for (uint8_t i = 0; i < VELOCITY_BUFFER_SIZE; i++){
    motor_vel[SIDE_L].data += motor_vel_buff[SIDE_L][i]; // mm/s
    motor_vel[SIDE_R].data += motor_vel_buff[SIDE_R][i]; // mm/s
  }
  motor_vel[SIDE_L].data /= VELOCITY_BUFFER_SIZE;
  motor_vel[SIDE_R].data /= VELOCITY_BUFFER_SIZE;
  vel_current[SIDE_L] = motor_vel[SIDE_L].data;
  vel_current[SIDE_R] = motor_vel[SIDE_R].data;

  // Store new encoder values
  encoder_val[SIDE_L].data = new_enc_data[SIDE_L];
  encoder_val[SIDE_R].data = new_enc_data[SIDE_R];

  lastLoopTime_us += delta_time;
}

/** Compute the feedforward PWM value (clipped at +/- 255) for a given velocity */
int16_t velFFCtrl(double vel){
  // Velocity of zero will always be 0 PWM
  if (vel == 0) return 0;

  // Get index of correct linear range
  uint8_t linear_index = 1;
  for(linear_index = 1; linear_index < lut_pwm_to_vel_n-1; linear_index++){
    if (vel <= lut_pwm_to_vel[linear_index]){
      break;
    }
  }
  
  // Compute the linear interpolation between the two active datapoints
  const int16_t pwm_lesser = (linear_index-1)*10 - 250; //linear_index=1 => pwm_lesser=-250, linear_index=31 => pwm_lesser=50
  const int16_t pwm_delta = 10;
  const double vel_lesser = lut_pwm_to_vel[linear_index-1];
  const double vel_delta = lut_pwm_to_vel[linear_index]-vel_lesser;
  double ff_pwm = (pwm_delta/vel_delta)*(vel-vel_lesser) + pwm_lesser;

  // Clip and return
  return constrain(ff_pwm, -255, 255);
}

/** Tick PID controllers on both wheels and read new PWM values into pwm_vals array.
 * Only runs if velocity has been set. Else, does nothing. */
void computePWMValues(){
  if (velocity_set){
    for(int motor_side = 0; motor_side<2; motor_side++){
      if(abs(vel_desired_prev[motor_side] - vel_desired[motor_side]) > 150){
        vel_pid[motor_side].reset();
      }
      vel_desired_prev[motor_side] = vel_desired[motor_side];
      const int16_t ff_pwm = velFFCtrl(vel_desired[motor_side]);
      if (ff_pwm == 0) {
        pwm_vals[motor_side] = 0;
      } else {
        vel_pid[motor_side].compute();
        pwm_vals[motor_side] = constrain(ff_pwm + (int16_t)pwm_vals_double[motor_side], -255, 255);
      }
    }
  }
}

/** Write the corrisponding pwm values to both wheels, adjusting the dir pins to match the sign. */
void writeToMotor(){
  for(int motor_side = 0; motor_side<2; motor_side++){
    const uint8_t dir1 = (motor_side == SIDE_L ? PIN_MOTOR_L_DIR_1 : PIN_MOTOR_R_DIR_1);
    const uint8_t dir2 = (motor_side == SIDE_L ? PIN_MOTOR_L_DIR_2 : PIN_MOTOR_R_DIR_2);
    const uint8_t motor_pin  = (motor_side == SIDE_L ? PIN_MOTOR_L_ENA   : PIN_MOTOR_R_ENB);
    if(pwm_vals[motor_side] < 0)
    {
      digitalWrite(dir1, LOW); 
      digitalWrite(dir2, HIGH);
    } 
    else 
    {
      digitalWrite(dir1, HIGH); 
      digitalWrite(dir2, LOW);
    }
    analogWrite(motor_pin, abs(pwm_vals[motor_side])); 
  }
}

/** Initialize IMU using Adafruit Library plugin and specify data type */
void setIMU(){
  bno08x.begin_I2C(); //Begin I2C
  bno08x.enableReport(SH2_GAME_ROTATION_VECTOR); // Define the sensor outputs you want to receive -- full list located in sh2.h line 85
  delay(1000);
}

/** Update IMU data values */
void readIMU(){
  bno08x.getSensorEvent(&sensorValue);
  imu_values[0] = sensorValue.un.rotationVector.real;
  imu_values[1] = sensorValue.un.rotationVector.i;
  imu_values[2] = sensorValue.un.rotationVector.j;
  imu_values[3] = sensorValue.un.rotationVector.k;

  rotation_vector.data = imu_values;
  rotation_vector.data_length = 4;
}

/** Update publishers and subscribers and spin ros */
void spinROS(){
  encoder_pub[SIDE_L].publish(&encoder_val[SIDE_L]);
  encoder_pub[SIDE_R].publish(&encoder_val[SIDE_R]);
  motor_vel_pub[SIDE_L].publish(&motor_vel[SIDE_L]);
  motor_vel_pub[SIDE_R].publish(&motor_vel[SIDE_R]);
  imu_pub.publish(&rotation_vector);
  ros_node_handle.spinOnce();
}




