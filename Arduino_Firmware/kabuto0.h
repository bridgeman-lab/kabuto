
#define ROBOT_NAME "robot_0" /** <=== Change name here to match robot getting flashed to */

const uint8_t lut_pwm_to_vel_n = 51;

const double lut_pwm_to_vel[lut_pwm_to_vel_n] = {
  -600.00, -596.00, -592.00, -587.00, -581.00, -576.00, -570.00, -562.00, -550.00, -540.00, // PWM -250 to -160
  -527.00, -512.00, -492.00, -475.00, -447.00, -421.00, -383.00, -342.00, -283.00, -217.00, // PWM -150 to -60
  -136.00,  -32.00,   -0.02,    0.00,    0.00,    0.00,    0.00,    0.00,    0.00,   42.00, 122.00, // PWM -50 to 50
   202.00,  269.00,  323.00,  367.00,  400.00,  429.00,  458.00,  479.00,  597.00,  510.00, // PWM 60 to 150
   523.00,  536.00,  547.00,  552.00,  562.00,  571.00,  578.00,  583.00,  587.00,  595.00}; // PWM 160 to 250